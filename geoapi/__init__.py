# (C) 2021 Pawel Weber All Rights Reserved.
from .lambda_class import LambdaBase
from .login import Login
from .geolocation import PostGeolocation, GetGeolocation, DeleteGeolocation
from .auth import Authenticate

# Auth handler
auth = Authenticate.get_handler()

# Endpoints
login_post = Login.get_handler()
geolocation_post = PostGeolocation.get_handler()
geolocation_get = GetGeolocation.get_handler()
geolocation_delete = DeleteGeolocation.get_handler()
