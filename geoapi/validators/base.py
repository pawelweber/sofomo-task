# (C) 2021 Pawel Weber All Rights Reserved.
from abc import ABC

import jsonschema
from jsonschema.exceptions import ValidationError
from geoapi.responses.error import HttpBadRequest


# pylint: disable=too-few-public-methods
class BaseValidator(ABC):
    schema = None

    def validate(self, instance: dict) -> None:
        """Validate dict using defined schema.

        Args:
            instance (dict): dictionary to validate

        Raises:
            Http400BadRequest: Raise on validation error
        """
        try:
            jsonschema.validate(instance=instance, schema=self.schema)
        except ValidationError as error:
            raise HttpBadRequest.from_error_message(error_msg=error.message)
