# (C) 2021 Pawel Weber All Rights Reserved.
from . import BaseValidator


# pylint: disable=too-few-public-methods
class GeolocationPostValidator(BaseValidator):
    schema = {
        'type': 'object',
        'required': ['address'],
        'properties': {
            'address': {
                'type': 'string',
            },
        }
    }
