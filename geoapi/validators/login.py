# (C) 2021 Pawel Weber All Rights Reserved.
from . import BaseValidator


# pylint: disable=too-few-public-methods
class LoginValidator(BaseValidator):
    schema = {
        'type': 'object',
        'required': ['username', 'password'],
        'properties': {
            'username': {
                'type': 'string',
            },
            'password': {
                'type': 'string'
            }
        },
    }
