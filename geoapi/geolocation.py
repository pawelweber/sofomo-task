# (C) 2021 Pawel Weber  All Rights Reserved.
import os

import requests
from boto3.dynamodb.conditions import Key

from geoapi import LambdaBase
from .responses import HttpResponse, HttpOk
from .validators import GeolocationPostValidator
from .responses import HttpBadRequest
from geoapi.auth import get_user_id

GEO_API_KEY = os.environ['GEO_API_KEY']
TABLE_NAME = 'Geolocation'


class PostGeolocation(LambdaBase):

    validator_class = GeolocationPostValidator

    def _prepare_geolocation_data(self, user_id: str, address: str, location: dict):
        location['latitude'] = str(location['latitude'])
        location['longitude'] = str(location['longitude'])
        data = {
            'PK': f'USER#{user_id}',
            'SK': f'GEOLOCATION#{address}',
            'createdAt': self.db_resource.get_datetime(),
            'location': location,
        }
        return data

    def handle(self, event: dict, context: object) -> HttpResponse:  # pylint disable=unused-argument
        request_body = self.get_request_body(event)
        self.validate_request_body(request_body)
        address = request_body['address']
        location = self.get_location_data(address)
        data = self._prepare_geolocation_data(get_user_id(event), address, location)
        self.db_resource.put_items(data, table_name=TABLE_NAME)
        return HttpOk()

    @staticmethod
    def get_location_data(address: str):
        location_req = requests.post(f'http://api.ipstack.com/{address}?access_key={GEO_API_KEY}')
        if not location_req.json()['type']:
            raise HttpBadRequest.from_error_message(f'Address {address} is not valid address')
        return location_req.json()


class GetGeolocation(LambdaBase):

    validator_class = GeolocationPostValidator

    @staticmethod
    def _parse_geolocation_data(address: str, query_result: dict):
        if not query_result or query_result[0] is {}:
            return {'info': f'No such address {address} in database. Add it first'}
        return {
            'address': address,
            'location': query_result[0].get('location')
        }

    def get_geolocation(self, user_id, ip_address):
        query_key_condition = Key('PK').eq(f'USER#{user_id}') & Key('SK').eq(f'GEOLOCATION#{ip_address}')
        return self.db_resource.query(query_key_condition, table_name=TABLE_NAME)

    def handle(self, event: dict, context: object) -> HttpResponse:  # pylint disable=unused-argument
        request_body = self.get_request_body(event)
        self.validate_request_body(request_body)
        geolocation = self.get_geolocation(get_user_id(event), request_body['address'])
        print(geolocation)
        return HttpResponse(self._parse_geolocation_data(request_body['address'], geolocation))


class DeleteGeolocation(LambdaBase):

    validator_class = GeolocationPostValidator

    @staticmethod
    def _parse_geolocation_data(address: str, query_result: dict):
        if not query_result or query_result[0] is {}:
            return {'info': f'No such address {address} in database. Add it first'}
        return {
            'address': address,
            'location': query_result[0].get('location')
        }

    def remove_geolocation(self, user_id, address):
        return self.db_resource.delete_item({'PK': f'USER#{user_id}', 'SK': f'GEOLOCATION#{address}'},
                                            table_name=TABLE_NAME)

    def handle(self, event: dict, context: object) -> HttpResponse:  # pylint disable=unused-argument
        request_body = self.get_request_body(event)
        self.validate_request_body(request_body)
        result = self.remove_geolocation(get_user_id(event), request_body['address'])
        print(result)
        return HttpOk()
