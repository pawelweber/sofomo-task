# (C) 2021 Pawel Weber All Rights Reserved.
import json
import logging
from abc import ABC, abstractmethod
from typing import Optional

from db_config.database_errors import DatabaseClientError, DatabaseConnectionError
from db_config.dynamodb_resource import DynamoDBResource
from .responses import HttpError, HttpBadRequest, HttpResponse

LOGGER = logging.getLogger(__name__)


class LambdaBase(ABC):
    """Base class for implementing Lambda handlers as classes.
    Used across multiple Lambda functions (included in each zip file).
    Add additional features here common to all your Lambdas, like logging.
    """
    validator_class = None
    db_resource = DynamoDBResource()

    @classmethod
    def get_handler(cls, *args, **kwargs):
        def handler(event, context):
            try:
                response = cls(*args, **kwargs).handle(event, context)
                return response.response if isinstance(response, HttpResponse) else response
            except HttpError as exception:
                return exception.response
            except (DatabaseClientError, DatabaseConnectionError) as exception:
                return HttpError.from_status_code(500, {"error": f"Database error {exception}"})
        return handler

    @abstractmethod
    def handle(self, event: dict, context: object) -> HttpResponse:
        raise NotImplementedError

    def get_validator(self):
        return self.validator_class()

    def validate_request_body(self, body: dict) -> None:
        if not self.validator_class:
            raise NotImplementedError('Class validator not found')
        self.get_validator().validate(body)

    @staticmethod
    def get_request_body(event: dict, json_decode=True) -> Optional[dict]:
        request_body = event.get('body')
        if json_decode:
            try:
                return json.loads(request_body)
            except json.JSONDecodeError as error:
                msg = f'Can not decode request body "{request_body}", error: "{error}"'
                LOGGER.error(msg)
                raise HttpBadRequest.from_error_message(error_msg=msg)
        return request_body
