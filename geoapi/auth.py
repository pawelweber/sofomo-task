# (C) 2021 Pawel Weber All Rights Reserved.
import os

import jwt
from jwt import PyJWKClient

from .lambda_class import LambdaBase
from .responses.error import HttpBadRequest, HttpUnauthorized

AUDIENCE = os.environ['AUDIENCE']
JWKS_URL = os.environ['JWKS_URL']


# pylint: disable=too-few-public-methods
class Authenticate(LambdaBase):

    def handle(self, event: dict, context: object) -> dict:  # pylint disable=unused-argument
        token_method, auth_token = self.__parse_token(event)
        self.__verify_token_structure(token_method, auth_token)
        return self.__authorize(event, auth_token)

    @staticmethod
    def __parse_token(event: dict) -> tuple:
        whole_auth_token = event.get('authorizationToken', '')
        token_parts = whole_auth_token.split()
        try:
            token_method = token_parts[0]
            auth_token = token_parts[1]
        except IndexError as exception:
            raise HttpBadRequest.from_error_message(f'Can not get token: {exception.__repr__()}')
        return token_method, auth_token

    @staticmethod
    def __verify_token_structure(token_method: str, auth_token: str):
        if token_method.lower() != 'bearer' or not auth_token or auth_token == 'null':
            raise HttpUnauthorized.from_error_message('Failing due to invalid token_method or missing auth_token')

    def __authorize(self, event: dict, auth_token: str) -> dict:
        try:
            payload = self.__jwt_verify(auth_token)
            return self.__generate_policy(payload, 'Allow', event.get('methodArn'))
        except Exception as exception:
            raise HttpUnauthorized.from_error_message(exception.__repr__())

    @staticmethod
    def __jwt_verify(auth_token: str) -> dict:
        pub_key = PyJWKClient(JWKS_URL).get_signing_key_from_jwt(auth_token)
        return jwt.decode(auth_token, pub_key.key, algorithms=['RS256'], audience=AUDIENCE)

    @staticmethod
    def __generate_policy(payload: dict, effect: str, resource: str) -> dict:
        acl = AccessList(resource, payload['permissions']).generate()
        return {
            'principalId': payload['sub'],
            'context': {
                'email': payload.get('email')
            },
            'policyDocument': {
                'Version': '2012-10-17',
                'Statement': [{
                    'Action': 'execute-api:Invoke',
                    'Effect': effect,
                    'Resource': acl
                }]
            }}


GEOLOCATION = {
    "read": [
        "GET/api/geolocation"
    ],
    "write": [
        "GET/api/geolocation",
        "POST/api/geolocation",
        "DELETE/api/geolocation"
    ]
}


ACCESS_RULES = {
    "geolocation": GEOLOCATION,
}


class AccessList:
    """
    Class AccessList creates the user access list for presented token. The token payload - permission is read to create
    list needed by the AWS Gateway API policy which is returned by the authenticator.
    """

    def __init__(self, resource: str, permissions: list):
        """
        AccessList init method parses the called resource and permissions list to create correct access list needed to
        create correct policy for AWS Gateway API.

        :param resource: Called method arn
        :param permissions: List of permissions from token payload
        """
        self.api_arn = self.__get_gateway_api_arn(resource)
        self.permissions = self.__parse_permissions(permissions)

    def generate(self) -> list:
        """
        Method generates list of accessible AWS API Gateway methods arns

        :return: List of AWS API Gateway methods arns
        """
        acl = list()
        for func, lvl in self.permissions.items():
            for endpoint in ACCESS_RULES[func][lvl]:
                acl.append(f'{self.api_arn}/{endpoint}')
        return acl

    @staticmethod
    def __parse_permissions(permissions) -> dict:
        return {permission.split(":")[1]: permission.split(":")[0] for permission in permissions}

    @staticmethod
    def __get_gateway_api_arn(resource: str) -> str:
        seg = resource.split("/")
        return f"{seg[0]}/{seg[1]}"


def get_user_id(event: dict) -> str:
    """
    Method reads user id from access token

    :param event: serverless request event

    :raises Http400BadRequest - Email not found in the event.

    :return user_id
    """
    try:
        return event['requestContext']['authorizer']['principalId'][6:]
    except KeyError:
        raise HttpBadRequest.from_error_message('User id not found in the request. Please validate JWT token.')
