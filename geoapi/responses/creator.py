# (C) 2021 Pawel Weber All Rights Reserved.
import decimal
import json
from typing import Generator


class HttpResponse:  # pylint: disable=too-few-public-methods
    status_code = 200
    body = {'message': 'OK'}
    header = {
        'Access-Control-Allow-Origin': '*',  # Required for CORS support to work
        'Access-Control-Allow-Credentials': True,  # Required for cookies, authorization headers with HTTPS
    }

    def __init__(self, body: dict = None):
        body = body or self.body
        self.response = self.create_aws_lambda_response(self.status_code, body)

    def create_aws_lambda_response(self, status_code: int, body: dict, headers: dict = None) -> dict:
        return {
            'statusCode': status_code,
            'headers': headers or self.header,
            'body': json.dumps(body, cls=DecimalEncoder)
        }


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return (str(o) for o in [o])
        if isinstance(o, Generator):
            return ""
        return super(DecimalEncoder, self).default(o)
