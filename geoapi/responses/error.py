# (C) 2021 Pawel Weber All Rights Reserved.
import logging

from .creator import HttpResponse

LOGGER = logging.getLogger(__name__)


class HttpError(HttpResponse, Exception):  # pylint: disable=too-few-public-methods
    status_code = 500
    body = {'error': 'Internal Server Error'}

    def __init__(self, **kwargs):
        if 'body' in kwargs:
            self.body = kwargs['body']
        if 'status_code' in kwargs:
            self.status_code = kwargs['status_code']
        self._log_error()
        super().__init__(self.body)

    def _log_error(self):
        log = f"Error response - status_code: {self.status_code}, error: {self.body['error']}"
        LOGGER.error(log)

    @classmethod
    def from_status_code(cls, status_code: int, body: dict):
        return cls(body=body, status_code=status_code)

    @classmethod
    def from_error_message(cls, error_msg: str):
        body = {
            'error': error_msg
        }
        return cls(body=body)


class HttpBadRequest(HttpError):  # pylint: disable=too-few-public-methods
    status_code = 400
    body = {'error': 'Bad Request'}


class HttpUnauthorized(HttpError):  # pylint: disable=too-few-public-methods
    status_code = 401
    body = {'error': 'Unauthorized'}
