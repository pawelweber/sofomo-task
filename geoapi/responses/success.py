# (C) 2021 Pawel Weber All Rights Reserved.

from .creator import HttpResponse


class HttpOk(HttpResponse):  # pylint: disable=too-few-public-methods
    pass
