# (C) 2021 Pawel Weber All Rights Reserved.
import os

import requests

from . import LambdaBase
from .responses import HttpResponse, HttpOk, HttpError
from .validators import LoginValidator


class Login(LambdaBase):

    validator_class = LoginValidator

    def handle(self, event: dict, context: dict) -> HttpResponse:
        body = self.get_request_body(event)
        self.validate_request_body(body)
        request_body = {
            'username': body['username'],
            'password': body['password'],
            'grant_type': 'password',
            'client_id': os.environ['CLIENT_ID'],
            'audience': os.environ['AUDIENCE']
        }
        response = requests.post(os.environ['TOKEN_URL'], data=request_body)
        if response.status_code == 200:
            body = {
                'access_token': response.json()['access_token']
            }
            return HttpOk(body)
        raise HttpError.from_status_code(response.status_code, response.json())
