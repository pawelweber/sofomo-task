# (C) 2021 Pawel Weber All Rights Reserved.
import json
from unittest import TestCase
from unittest.mock import patch

from geoapi import auth
from tests import api_gateway_event


class TestAuthEndpoint(TestCase):

    @patch('geoapi.Authenticate._Authenticate__jwt_verify', return_value={'sub': 123, 'email': 'email@email.com',
                                                    'permissions': ["write:geolocation"]})
    def test_auth_pass_proper_token_grand_access_successfully(self, mock_jwt_verify):
        event = api_gateway_event({})
        policy = auth(event, None)
        expected_policy = {
            'principalId': 123,
            'context': {
                'email': 'email@email.com'
            },
            'policyDocument': {
                'Version': '2012-10-17',
                'Statement': [{
                    'Action': 'execute-api:Invoke',
                    'Effect': 'Allow',
                    'Resource': ['someawsarn/dev/GET/geolocation',
                                 'someawsarn/dev/POST/geolocation',
                                 'someawsarn/dev/DELETE/geolocation']
                }]
            }}
        self.assertEqual(policy, expected_policy)

    @patch('geoapi.Authenticate._Authenticate__jwt_verify', side_effect=Exception)
    def test_auth_unexpected_error_during_jwt_verification_response_error(self, _):
        response = auth(api_gateway_event({}), None)
        self.assertEqual(response['statusCode'], 401)

    def test_auth_without_event_response_error(self):
        response = auth({}, None)
        self.assertEqual(response['statusCode'], 400)
        assert 'Can not get token' in json.loads(response['body'])['error']

    def test_auth_without_without_token_response_error(self):
        event = api_gateway_event({})
        event['authorizationToken'] = 'b x'
        response = auth(event, None)
        self.assertEqual(response['statusCode'], 401)
        self.assertEqual(json.loads(response['body']),
                         {'error': 'Failing due to invalid token_method or missing auth_token'})
