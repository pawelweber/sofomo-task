# (C) 2021 Pawel Weber All Rights Reserved.
import json

from unittest.mock import patch, Mock

from geoapi import login_post


class TestLogin:

    @patch('requests.post',
           return_value=Mock(status_code=200, json=Mock(return_value={'access_token': 'sometoken'})))
    def test_login_with_correct_request(self, post_mock):  # pylint disable=unused-argument
        body = {'body': '{"username": "fakeuser", "password": "fakepassword"}'}
        response = login_post(body, None)
        assert response['statusCode'] == 200
        assert json.loads(response['body']) == {'access_token': 'sometoken'}

    def test_login_no_username(self):
        body = {'body': '{"password": "fakepassword"}'}
        response = login_post(body, None)
        assert response['statusCode'] == 400

    @patch('requests.post',
           return_value=Mock(status_code=403, json=Mock(return_value={'error': 'invalid password'})))
    def test_auth0_error(self, post_mock):
        body = {'body': '{"username": "fakeuser", "password": "fakepassword"}'}
        response = login_post(body, None)
        assert response['statusCode'] == 403
        assert json.loads(response['body']) == {'error': 'invalid password'}
