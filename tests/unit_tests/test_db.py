# (C) 2021 Pawel Weber All Rights Reserved.
from unittest import mock
from unittest.mock import ANY, Mock

import pytest
from botocore.exceptions import EndpointConnectionError
from db_config import DatabaseConnectionError, DynamodbConnector


@mock.patch('boto3.resource')
def test_connect(mock_client):
    ddb = DynamodbConnector()
    ddb.connect()
    mock_client.assert_called_once_with('dynamodb', endpoint_url='https://dynamodb.eu-west-1.amazonaws.com:443',
                                        region_name='eu-west-1', config=ANY)


@mock.patch('boto3.resource', mock.MagicMock())
def test_disconnect():
    ddb = DynamodbConnector()
    ddb.connect()
    ddb.disconnect()
    assert ddb.connection is None


@mock.patch('boto3.resource')
def test_check_connection_success(mock_client):
    conn = Mock()
    conn.list_tables = Mock()
    mock_client.return_value = conn
    ddb = DynamodbConnector()
    ddb.connect()
    ddb.check_connection()


@mock.patch('boto3.resource')
def test_check_connection_fail(mock_client):
    conn = Mock()
    endpoint_url = "dummy"
    conn.meta.client.list_tables = Mock(side_effect=EndpointConnectionError(endpoint_url=endpoint_url))
    mock_client.return_value = conn
    ddb = DynamodbConnector()
    ddb.connect()
    with pytest.raises(DatabaseConnectionError) as excinfo:
        ddb.check_connection()
    assert f'Could not connect to the endpoint URL: "{endpoint_url}"' == str(excinfo.value)
