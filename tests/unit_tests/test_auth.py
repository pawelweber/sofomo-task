# (C) 2021 Pawel Weber All Rights Reserved.

from pytest import raises

from geoapi.auth import get_user_id
from geoapi.responses.error import HttpBadRequest
from tests import api_gateway_event


def test_get_user_email_successfully():
    event = api_gateway_event({})
    assert get_user_id(event) == event['requestContext']['authorizer']['principalId'][6:]


def test_get_non_existing_user_email_raises_http_error():
    event = api_gateway_event({})
    event['requestContext']['authorizer'] = {}
    with raises(HttpBadRequest):
        get_user_id(event)
