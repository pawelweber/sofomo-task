# (C) 2021 Pawel Weber All Rights Reserved.
import json


def api_gateway_event(payload: dict = None) -> dict:
    body = json.dumps(payload) or payload
    return {
        'resource': '',
        'path': '',
        'httpMethod': '',
        "methodArn": 'someawsarn/dev/POST/goal',
        'headers': {},
        'requestContext': {
            'resourcePath': '',
            'httpMethod': '',
            'authorizer': {
                'principalId': 'auth0|5fd0aa3b63436700752e020e'
            }
        },
        'authorizationToken': 'Bearer TOKEN',
        'body': body
    }
