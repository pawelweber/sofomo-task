# (C) 2021 Pawel Weber All Rights Reserved.
from abc import ABC, abstractmethod
from typing import List, Union


class DatabaseResource(ABC):

    @abstractmethod
    def put_items(self, data: Union[List[dict], dict], table_name: str) -> None:
        """Create new or overwrite existing items

        Args:
            data (Union[list(dict), dict]): item or list of items to add/update
            table_name (str): put data to the table with this name
        """

    @abstractmethod
    def query(self, condition_expression, table_name: str) -> list:
        """Make query to database

        Args:
            condition_expression : expression to be use to query
            table_name (str): get data from table with this name

        Returns:
            list: List of queried items. Can be empty.
        """

    @abstractmethod
    def update_item(self, key: dict, body: dict, table_name: str):
        """Update item attributes.

        Args:
            key (dict): item key to update
            body (dict): attribute with values to update
            table_name (str): table name
        """
