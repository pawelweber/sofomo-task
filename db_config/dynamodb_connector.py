# (C) 2021 Pawel Weber All Rights Reserved.
import os
from typing import Type

import boto3
from botocore.config import Config
from botocore.exceptions import EndpointConnectionError

from db_config import CloudConfig, DatabaseConnectionError, IDatabaseConnector, TestConfig

DynamoResourceType = Type[boto3.session.Session.resource]

DB_CONFIG = TestConfig if os.environ['STAGE'] == 'local' else CloudConfig


class DynamodbConnector(IDatabaseConnector):

    def __init__(self, db_url=DB_CONFIG.DB_URL, region=DB_CONFIG.REGION):
        self.connection: DynamoResourceType = None
        self.db_url = db_url
        self.region = region
        self.stage = os.environ['STAGE']

    def connect(self):
        config = Config(connect_timeout=1, read_timeout=1, retries={'max_attempts': 1})

        self.connection = boto3.resource('dynamodb', endpoint_url=self.db_url,
                                         region_name=self.region,
                                         config=config)

    def check_connection(self):
        try:
            self.connection.meta.client.list_tables()
        except EndpointConnectionError as exception:
            raise DatabaseConnectionError(exception)

    def disconnect(self):
        self.connection = None
