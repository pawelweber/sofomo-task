# (C) 2021 Pawel Weber All Rights Reserved.
from abc import ABC, abstractmethod


class IDatabaseConnector(ABC):

    @abstractmethod
    def connect(self):
        """Create connection with database"""

    @abstractmethod
    def check_connection(self):
        """Check connection with database. Can raise DatabaseConnectionError if connection not established"""

    @abstractmethod
    def disconnect(self):
        """Close connection with database"""
