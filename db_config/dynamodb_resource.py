# (C) 2021 Pawel Weber All Rights Reserved..

import uuid
from datetime import datetime, timezone
from typing import List, Tuple, Union

from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError

from .database_errors import DatabaseClientError
from .database_resource import DatabaseResource
from .dynamodb_connector import DynamodbConnector


class DynamoDBResource(DatabaseResource):
    """
    Resources to help with operations on DynamoDB.
    Class handle connection with database only when it is needed to save initialization time.
    """

    def __init__(self):
        self.database_connector = DynamodbConnector()

    # pylint: disable=no-self-argument
    def _database_operation(method):
        """Connect with a database if needed. Use for decorating methods which operate on a database."""

        def wrapper(self, *args, **kwargs):
            try:
                if not self.database_connector.connection:
                    self.database_connector.connect()
                return method(self, *args, **kwargs)
            except Exception as exception:
                raise DatabaseClientError(exception)
        return wrapper

    @_database_operation
    def put_items(self, data: Union[List[dict], dict], table_name: str) -> None:
        table = self.database_connector.connection.Table(f'{table_name}-{self.database_connector.stage}')
        if type(data) is list:  # pylint: disable=unidiomatic-typecheck
            with table.batch_writer() as batch:
                for item in data:
                    batch.put_item(Item=item)
        else:
            table.put_item(Item=data)

    @_database_operation
    def query(self, key_condition_expression: Key, table_name: str, global_secondary_index: str = None) -> list:
        table = self.database_connector.connection.Table(f'{table_name}-{self.database_connector.stage}')
        if global_secondary_index:
            response = table.query(IndexName=global_secondary_index,
                                   KeyConditionExpression=key_condition_expression)
        else:
            response = table.query(KeyConditionExpression=key_condition_expression)
        return response.get('Items', [])

    @_database_operation
    def delete_item(self, key: dict, table_name: str):
        table = self.database_connector.connection.Table(f'{table_name}-{self.database_connector.stage}')
        table.delete_item(Key=key)

    @_database_operation
    def update_item(self, key: dict, body: dict, table_name: str):
        table = self.database_connector.connection.Table(f'{table_name}-{self.database_connector.stage}')
        expression, attributes, values = self._get_update_params(body)
        table.update_item(Key=key, UpdateExpression=expression, ExpressionAttributeNames=attributes,
                          ExpressionAttributeValues=values)

    def _get_update_params(self, body: dict) -> Tuple[str, dict]:
        """Generate an update expression and a dict of values to update a dynamodb item.

        Params:
            body (dict): Parameters to use for formatting.

        Returns:
            update_attributes, dict of attributes to update
            update expression, dict of values.
        """
        update_expression = ['set ']
        update_attributes = dict()
        update_values = dict()
        for key, val in body.items():
            attribute = f'#_{key}'
            update_expression.append(f' {attribute} = :{key},')
            update_attributes[attribute] = key
            update_values[f':{key}'] = val
        return ''.join(update_expression)[:-1], update_attributes, update_values

    @staticmethod
    def generate_id():
        return uuid.uuid1().hex

    @staticmethod
    def get_datetime():
        return datetime.now(tz=timezone.utc).isoformat()
