# (C) 2021 Pawel Weber All Rights Reserved.

# pylint: disable=too-few-public-methods
class TestConfig:
    DB_HOST = 'localhost'
    DB_PORT = '8000'
    DB_URL = f'http://{DB_HOST}:{DB_PORT}'
    REGION = 'dummy'


# pylint: disable=too-few-public-methods
class CloudConfig:
    DB_HOST = 'dynamodb.eu-west-1.amazonaws.com'
    DB_PORT = '443'
    DB_URL = f'https://{DB_HOST}:{DB_PORT}'
    REGION = 'eu-west-1'
