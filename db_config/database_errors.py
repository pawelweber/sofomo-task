# (C) 2021 Pawel Weber All Rights Reserved..

class DatabaseConnectionError(Exception):
    pass


class DatabaseClientError(Exception):
    pass
