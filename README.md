# API REST gateway


### Run pylint

```bash
tox -e pylint
```

### Run pytest

```bash
tox -e test
```

## Serverless framework

For your convenience the serverless framework was containerized. Please follow
requirements subsection to install needed docker utilities before you begin.

### Prerequisites

To use the dockerized serveless framework please install on your host machine:

* Docker CE => `19.03.13-ce`
* docker-compose => `1.27.4`

### Setup docker container

To setup the docker container with serverless framework type:

```bash
./sls-docker --install
```

and next to validate installation:

```bash
./sls-docker --version
```

### Configure AWS serverless credentials

```bash
./sls-docker config credentials --provider aws --key XXXXXXXXX --secret XXXXXXXXXX --profile sofomo
```

### Deploy app

```bash
./sls-docker deploy
```

### API description

API description in OpenAPI format you will find in specification directory