# (C) 2021 Pawel Weber All Rights Reserved.
#!/bin/bash
export AWS_PROFILE=sofomo
cd /mnt/workspace && npm install && sls $@
